let skill = [
  'self',
  'ally',
  'all allies',
  'other allies',
  'random ally',
  'enemy',
  'all enemies',
  'random enemy',
  'all',
  'self and enemy'
].map(x => {
  return {
    label: x,
    value: x
  }
})

let effect = [
  'target',
  'target allies',
  // 'target ally',
  'target team',
  'caster',
  'caster allies',
  // 'caster ally',
  'caster team',
  'enemy',
  'enemy team'
].map(x => {
  return {
    label: x,
    value: x
  }
})

export default {
  skill,
  effect
}
